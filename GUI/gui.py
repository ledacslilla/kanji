# -*- coding: UTF-8  -*-


from kivymd.app import MDApp
import gc
import os.path as path
from kivy.uix.widget import Widget
from PIL import Image
from kivy.config import Config
from kanji_CNN import recognise, kanji_list
from web_scraping import lookup_kanji
from exception_handler import exceptions
from widgets import drawingcanvas, historytabs
import clipboard







Config.set('input', 'mouse', 'mouse,multitouch_on_demand')





class Recogniser(Widget):

    def __init__(self):
        super().__init__()

    children_index = 1

    def create_dict_entry(self, kanji):
        try:
            furigana, meanings = lookup_kanji.check_data(kanji)
        except TypeError:
            raise TypeError
        else:
            KanjiRecogniserApp.kanji_dictionary[kanji] = {"furigana": furigana,
                                       "meanings": meanings}


    def search_kanji(self):
            try:
                self.create_dict_entry(self.ids.kanji.text)
            except TypeError:
                popup = exceptions.exception_handling(404)
                popup.open()
            else:
                KanjiHistory = historytabs.HistoryTabs(text=f'[font=assets/07YasashisaAntique]{self.ids.kanji.text}[/font]' ) #szélesség, magasság
                self.ids.history_tabs.add_widget(KanjiHistory)
                clipboard.copy(self.ids.kanji.text)
            finally:
                self.ids.kanji.text =''


    def add_kanji(self):
        p =  path.abspath(path.join(__file__, "../.."))
        save_path = p+"\\assets\screenshot.png"
        drawingcanvas.DrawingCanvas.export_to_png(self.ids.drawing_canvas, save_path)
        im = Image.open(save_path)
        left = 0
        top = 0
        right = self.ids.drawing_canvas.size[0]*0.9
        bottom = self.ids.drawing_canvas.size[1]*0.59
        im1 = im.crop((left, top, right, bottom))
        end_image = p+"\\test.png"
        im1.save(end_image)
        class_ = recognise.recogniseKanji(end_image)
        self.update_predictions(class_)
        drawingcanvas.DrawingCanvas.clear_canvas(self.ids.drawing_canvas)


    def undo(self):
        drawingcanvas.DrawingCanvas.undo_touch(self.ids.drawing_canvas)

    def clear_canvas(self):
        drawingcanvas.DrawingCanvas.clear_canvas(self.ids.drawing_canvas)

    def clear(self):
        for children in self.ids.history_tabs.children[0:]:
            self.ids.history_tabs.remove_widget(children)
        KanjiRecogniserApp.kanji_dictionary.clear()
        gc.collect()

    def delete_kanji(self):
        self.ids.kanji.text = ''

    def update_predictions(self,pred_array):
        self.ids.first_pred.text = kanji_list.label[pred_array[0][0]]
        self.ids.first_pred.tooltip_text = f'{pred_array[0][1]*100}%'
        self.ids.second_pred.text = kanji_list.label[pred_array[1][0]]
        self.ids.second_pred.tooltip_text = f'{pred_array[1][1] * 100}%'
        self.ids.third_pred.text = kanji_list.label[pred_array[2][0]]
        self.ids.third_pred.tooltip_text = f'{pred_array[2][1] * 100}%'

    def add_pred(self,number):
        if number ==1:
            self.ids.kanji.text += self.ids.first_pred.text
        elif number ==2:
            self.ids.kanji.text += self.ids.second_pred.text
        else:
            self.ids.kanji.text += self.ids.third_pred.text





class KanjiRecogniserApp(MDApp):
    kanji_dictionary = {}
    def build(self):
        return Recogniser()

