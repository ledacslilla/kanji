import urllib.parse
from bs4 import BeautifulSoup
import requests




def sparse_input(kanji):
    add_item = False
    new_input_array = []
    separator = ''
    for query in kanji:
        items = str(query)
        char_list = list(items)
        for chars in char_list:
            if chars == "<":
                add_item = False
            if add_item:
                new_input_array.append(chars)
            if chars == ">":
                add_item = True
    outp = separator.join(new_input_array)
    outp = outp.replace('\n\n',', ')
    return outp.strip()


def sparse_meaning_compound(meaning):
    add_item = False
    new_input_array = []
    separator = ''
    for query in meaning:
        items = str(query)
        char_list = list(items)
        for chars in char_list:
            if chars == "<":
                add_item = False
            if add_item:
                new_input_array.append(chars)
            if chars == ">":
                add_item = True
        new_input_array.append("; ")

    outp = separator.join(new_input_array[:-1])
    return outp

def sparse_meaning(meaning):
    separator = ', '
    mean = list(meaning)
    meaning1 = mean[0].strip()
    meaning_list = meaning1.split(', ')
    outp = separator.join(meaning_list)
    return outp



# gets the japanese characters, they are turned into bytestrings that are URL escaped by using urllib.parse
def get_info_from_web_compound(stringchar):
    query = stringchar.strip()
    url_query = urllib.parse.quote(query)
    URL = f'https://jisho.org/word/{url_query}'

    try:
        page = requests.get(URL)
        page.raise_for_status()
        soup = BeautifulSoup(page.text, 'html.parser')
        furigana = soup.find_all("span", class_="furigana")
        outp_furi = sparse_input(furigana)
        meaning = soup.find_all("span", class_="meaning-meaning")
        meaning_list = sparse_meaning_compound(meaning)
        return (outp_furi, meaning_list)
    except requests.exceptions.HTTPError as exception:
        return 404


def get_info_from_web(stringchar):
    query = stringchar.strip()
    url_query = urllib.parse.quote(query)
    URL = f'https://jisho.org/search/{url_query}%23kanji'
    print(URL)
    page = requests.get(URL)
    soup = BeautifulSoup(page.text, 'html.parser')
    furigana = soup.find_all("dd", class_="kanji-details__main-readings-list")
    outp_furi = sparse_input(furigana)
    meaning = soup.find("div", class_="kanji-details__main-meanings")
    meaning_list = sparse_meaning(meaning)
    return(outp_furi,meaning_list)

def check_data(kanji):
    if len(kanji) >1:
        furi, meaning = get_info_from_web_compound(kanji)
    else:
        furi, meaning = get_info_from_web(kanji)
    return(furi,meaning)

