from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivymd.uix.button import Button


def exception_handling(code):
    message = ''
    if code == 404:
        message = 'Unknown kanji compound, please provide another one'
    elif code == 'empty_canvas':
        message = 'The canvas is empty, please draw a kanji'
    elif code == 'no_kanji':
        message = 'There are no kanjis in the input bar yet, please choose a prediction'
    content = Button(text='Close',size_hint = (0.4,0.4))
    layout = GridLayout(rows=2, padding=10)
    layout.add_widget(Label(text=message))
    layout.add_widget(content)
    popup = Popup(title='Error', content=layout, size_hint=(0.6, 0.6),
                  auto_dismiss=False)
    content.bind(on_press=popup.dismiss)
    return popup