import struct
from PIL import Image, ImageEnhance
import os
from exclude import hiraganas
from pathlib import Path


outdir = "ETL8G-img/"
if not os.path.exists(outdir):
    os.mkdir(outdir)

def read_record_ETL8G(f):
    s = f.read(8199)
    r = struct.unpack('>2H8sI4B4H2B30x8128s11x', s)
    iF = Image.frombytes('F', (128, 127), r[14], 'bit', 4)
    iL = iF.convert('L')
    return r + (iL,)

def save_photo(img, fp):

    iF = Image.frombytes('F', (128, 127), img, 'bit', 4)
    iL = iF.convert('L')
    enhancer = ImageEnhance.Brightness(iL)
    iE = enhancer.enhance(16)
    iE.save(fp, 'PNG')



def read_kanji():
    for i in range(1, 33):
        filename = '../assets/ETL8G/ETL8G_{:02d}'.format(i)
        with open(filename, 'rb') as f:
            for dataset in range(5):
                char = 0
                for j in range(956):
                    r = read_record_ETL8G(f)
                    if str(r[2], 'utf-8') not in hiraganas:
                        dir = outdir + "/" + str(char)
                        if not os.path.exists(dir):
                            os.mkdir(dir)
                        fn = '{}.png'.format((i - 1) * 5 + dataset)
                        fullpath = dir + "/" + fn
                        save_photo(r[14],fullpath)
                        char += 1

read_kanji()