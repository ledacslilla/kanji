import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.preprocessing.image import ImageDataGenerator



rows = 96
cols = 96


x_train= np.load("kanji_train_images.npz")['arr_0']
y_train= np.load("kanji_train_labels.npz")['arr_0']
x_test = np.load("kanji_test_images.npz")['arr_0']
y_test = np.load("kanji_test_labels.npz")['arr_0']

shift = 0.2
datagen = ImageDataGenerator(rotation_range=15,zoom_range=0.2,width_shift_range=shift)
datagen.fit(x_train)



model = keras.Sequential([
    keras.layers.Conv2D(64, (3, 3),  padding='same', input_shape=x_train.shape[1:]),
    keras.layers.ReLU(),
    keras.layers.Conv2D(64, (3, 3)),
    keras.layers.ReLU(),
    keras.layers.MaxPooling2D(2, 2),
    keras.layers.Dropout(0.5),

    keras.layers.Conv2D(64, (3, 3),  padding='same'),
    keras.layers.ReLU(),
    keras.layers.Conv2D(64, (3, 3)),
    keras.layers.ReLU(),
    keras.layers.MaxPooling2D(2, 2),
    keras.layers.Dropout(0.5),

    keras.layers.Conv2D(128, (3, 3), padding='same'),
    keras.layers.ReLU(),
    keras.layers.Conv2D(128, (3, 3)),
    keras.layers.ReLU(),
    keras.layers.MaxPooling2D(2, 2),
    keras.layers.Dropout(0.5),

    keras.layers.Flatten(),
    keras.layers.Dropout(0.5),
    keras.layers.Dense(2048),
    keras.layers.ReLU(),
    keras.layers.Dense(10, activation= tf.keras.activations.softmax)
])


optimizer = keras.optimizers.Adam(lr=0.0001)
model.compile(loss="sparse_categorical_crossentropy", optimizer=optimizer, metrics= ['accuracy'])


model.fit(datagen.flow(x_train,y_train,shuffle=True),epochs=50,validation_data=(x_test,y_test),
                    callbacks=[keras.callbacks.EarlyStopping(patience=8, verbose=1, restore_best_weights=True),
                               keras.callbacks.ReduceLROnPlateau(factor=0.5, patience=3, verbose=1)])

model.save("kanji.h5")