import numpy as np
import os
import cv2
import sys
import random
from sklearn.model_selection import train_test_split


DATADIR = "ETL8G-img"
CATEGORIES = np.arange(10)

img_size = 96
training_data = []

original_stdout = sys.stdout
def create_training_data():
    for category in CATEGORIES:
        path = os.path.join(DATADIR, str(category))
        for img in os.listdir(path):
            img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
            new_array = cv2.resize(img_array, (img_size, img_size))
            with open('filename.txt', 'a') as f:
                np.set_printoptions(threshold=sys.maxsize)
                sys.stdout = f  # Change the standard output to the file we created.
                print(new_array)
                print(img)
                print()
                sys.stdout = original_stdout
            training_data.append([new_array, category])
        print(category)


create_training_data()


x = []
y = []

for features, label in training_data:
    x.append(features)
    y.append(label)

x = np.array(x).reshape(-1, img_size, img_size, 1)

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)


np.savez_compressed("kanji_train_images.npz", x_train)
np.savez_compressed("kanji_train_labels.npz", y_train)
np.savez_compressed("kanji_test_images.npz", x_test)
np.savez_compressed("kanji_test_labels.npz", y_test)

