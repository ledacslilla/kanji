import cv2
import os
import numpy as np
import tensorflow as tf
import sys

img_size = 96
def get_probability(prob):
    return prob[1]

def recogniseKanji(image):

    p = os.path.abspath('kanji_CNN\kanji.h5')
    t = os.path.abspath(image)
    print (t)
    model = tf.keras.models.load_model(p)
    im = cv2.imread(image,cv2.IMREAD_GRAYSCALE)
    im = im.astype(np.float32)
    test_image = cv2.resize(im,(img_size, img_size))
    np.set_printoptions(threshold=sys.maxsize)
    test_image2 = np.array(test_image).reshape(-1, img_size, img_size, 1)
    classes = model.predict(test_image2)
    temp = np.argpartition(-classes[0], 3)
    result_args = temp[:3]
    result = []
    for i in result_args:
        result.append([i,classes[0][i]])
    result.sort(key=get_probability,reverse = True)
    tf.keras.backend.clear_session()
    return result