from kivymd.uix.list import OneLineListItem
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivymd.uix.button import Button
from GUI import gui



class HistoryTabs(OneLineListItem):
    def see_hist(self):
        kan = list(self.text)
        copy = False
        new_input_array = []
        separator = ''
        for char in kan:
            if char == '[':
                copy = False
            if copy:
                new_input_array.append(char)
            if char == ']':
                copy = True

        kanji = separator.join(new_input_array)
        content = Button(text='Close', size_hint =(0.4,0.4))
        layout = GridLayout(rows=4, padding=10)
        layout.add_widget(Label(text=f'kanji: {kanji}',font_name = "assets/07YasashisaAntique" ,font_size = 20,text_size = self.size, color = (0,0,0,1), outline_color = (255/255.,255/255,255/255,1), outline_width = 2))
        layout.add_widget(Label(text=f'furigana: {gui.KanjiRecogniserApp.kanji_dictionary[kanji]["furigana"]}',font_name = "assets/07YasashisaAntique",text_size = self.size, color = (0,0,0,1), outline_color = (255/255.,255/255,255/255,1), outline_width = 2))
        layout.add_widget(Label(text=f'meaning: {gui.KanjiRecogniserApp.kanji_dictionary[kanji]["meanings"]}',text_size = self.size, color = (0,0,0,1), outline_color = (255/255.,255/255,255/255,1), outline_width = 2))
        layout.add_widget(content)
        popup = Popup(title='Kanji details', content=layout, background = "assets/bg1.jpg", size_hint= (0.6,0.6) , auto_dismiss=False)
        content.bind(on_press=popup.dismiss)
        popup.open()
