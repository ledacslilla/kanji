from kivy.graphics import InstructionGroup, Line, Color
from kivy.uix.widget import Widget


class DrawingCanvas(Widget):
    objects = []
    drawing = False
    is_empty = True

    def on_touch_up(self, touch):
        self.drawing = False

    def on_touch_move(self, touch):
        if self.drawing:
            self.points.append(touch.pos)
            self.obj.children[-1].points = self.points
            self.is_empty = False
        else:
            self.drawing = True
            self.points = [touch.pos]
            self.obj = InstructionGroup()
            self.obj.add(Color(1,1,1))
            self.obj.add(Line(width=4))
            self.objects.append(self.obj)
            self.canvas.add(self.obj)
            self.is_empty = False

    def undo_touch(self):
        if len(self.objects) ==0:
            self.is_empty = True
        else:
            item = self.objects.pop(-1)
            self.canvas.remove(item)

    def clear_canvas(self):
        for item in self.objects:
            self.canvas.remove(item)
            self.is_empty = True
